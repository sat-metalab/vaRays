# NORMES GRAPHIQUES 


## LOGOS PRIMAIRE ET SECONDAIRE

#### GRILLE GRAPHIQUE

Voici la grille graphique du logo, avec les proportions à respecter à chaque utilisation du logo. Que ce soit pour apparaître dans une application, sur un site web, sur une entête de lettre, sur une affiche ou dans une série de logos, les proportions doivent TOUJOURS êtres respectées.

![](img/graphic-grid.png)

#### ESPACE DE PROTECTION

En tout temps, le logo doit être entouré d’une zone dégagée pour être lisible et communiquer efficacement. Aucun texte, aucune ligne, aucun élément typographique, aucune image, photo ou illustration ne doit occuper cet espace.

![](img/protection-space-1.png)
![](img/protection-space-2.png)

#### TAILLE MINIMALE

Le logo ne doit jamais être utilisé lorsque l’icône principale est à moins de 0,5 pouces de hauteur.

![](img/minimum-size.png)

## CODES COULEURS

#### COULEURS (DÉGRADÉ)

![](../PNG/vaRays_C.png)
```
C2 M62 Y0 K0
R247 G100 B255
#f764ff

C1 M95 Y0 K0
R255 G0 B255
#ff00ff

C34 M94 Y0 K0
R165 G0 B169
#a500a9
```
#### NOIR

![](../PNG/vaRays_N.png)
```
C0 M0 Y0 K30
R179 G179 B179
#b3b3b3

C0 M0 Y0 K70
R77 G77 B77
#4d4d4d

C0 M0 Y0 K100
R0 G0 B0
#000000
```
#### BLANC

![](img/vaRays-white-with-black-background.png)
```
C0 M0 Y0 K0
R255 G255 B255
#ffffff

C0 M0 Y0 K40
R153 G153 B153
#999999

C0 M0 Y0 K70
R77 G77 B77
#4d4d4d
```
## UTILISATIONS GÉNÉRALES

### Fond uni et foncé

Pour maximiser la lisibilité du logo sur un fond uni et foncé, il est conseillé d’utiliser la version couleur ou blanche du logo.

JAMAIS utiliser la version noire.

![](img/plain-and-dark-background.png)


### Fond uni et clair

Pour maximiser la lisibilité du logo sur un fond uni et clair, uniquement la version couleur ou noire du logo est permise.

JAMAIS utiliser la version blanche.

![](img/plain-and-clear-background.png)


### Photo très claire

Pour maximiser la lisibilité du logo sur une photo très claire, il est conseillé d’utiliser la version noire du logo. 

JAMAIS utiliser la version blanche.

![](img/very-clear-photo.png)

## Lisibilité maximale

Pour assurer une lisibilité maximale du logo, la règle à respecter est de ne jamais modifier ni altérer aucune partie du logo.
