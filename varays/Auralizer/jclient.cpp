//  -----------------------------------------------------------------------------
//  Copyright (C) 2019- [SAT] Metalab (Harish Venkatesan)
//  Copyright (C) 2006-2011 Fons Adriaensen <fons@linuxaudio.org>
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
//  -----------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "./jclient.hpp"

Jclient::Jclient(const std::string& jname, const std::string& jserv, Convproc* convproc, DopplerEngine* dopplerEngine)
    : _jack_client(0)
    , _jname(jname)
    , _fsamp(0)
    , _fragm(0)
    , _flags(0)
    , _clear(false)
    , _ninp(0)
    , _nout(0)
    , _convproc(convproc)
    , _dopplerEngine(dopplerEngine)
    , _initSuccess(false)
#ifdef __CONV_TEST__
    , _convsync(true)
#else
    , _convsync(false)
#endif

{
    if (init_jack(jname, jserv))
        _initSuccess = true;
}

Jclient::~Jclient(void)
{
    stop();
    if (_jack_client)
        close_jack();
}

int Jclient::delete_ports(void)
{
    unsigned int k;

    _ninp = 0;
    _nout = 0;

    for (const auto& it : _activeInputPorts)
        jack_port_unregister(_jack_client, it.second);
    _activeInputPorts.clear();

    for (k = 0; k < Convproc::MAXOUT; k++)
    {
        if (_jack_outpp[k])
        {
            jack_port_unregister(_jack_client, _jack_outpp[k]);
            _jack_outpp[k] = 0;
        }
    }

    return 0;
}

int Jclient::delete_port(const std::string& name)
{
    auto it = _activeInputPorts.find(name);
    if (it != _activeInputPorts.end())
    {
        if (jack_port_unregister(_jack_client, it->second))
        {
            logger_.log(Logger::ERROR, "Unable to remove port named " + name);
            return -1;
        }
        _activeInputPorts.erase(it);
    }
    return 0;
}

bool Jclient::init_jack(const std::string& jname, const std::string& jserv)
{
    struct sched_param spar;
    jack_status_t stat;
    int opts;

    opts = JackNoStartServer;
    if (!jserv.empty())
        opts |= JackServerName;
    if ((_jack_client = jack_client_open(jname.c_str(), (jack_options_t)opts, &stat, jserv.c_str())) == 0)
    {
        logger_.log(Logger::CRITICAL, "Can't connect to JACK server " + jserv);
        return false;
    }

    jack_on_shutdown(_jack_client, jack_static_shutdown, (void*)this);
    jack_set_freewheel_callback(_jack_client, jack_static_freewheel, (void*)this);
    jack_set_buffer_size_callback(_jack_client, jack_static_buffsize, (void*)this);
#ifndef __CONV_TEST__
    jack_set_process_callback(_jack_client, jack_static_process, (void*)this);
#endif

    if (jack_activate(_jack_client))
    {
        logger_.log(Logger::CRITICAL, "Can't activate JACK.");
        return false;
    }

    _jname = jack_get_client_name(_jack_client);
    _fsamp = jack_get_sample_rate(_jack_client);
    _fragm = jack_get_buffer_size(_jack_client);
    if (_fragm < 32)
    {
        logger_.log(Logger::WARNING, "JACK buffer size is too small. Setting it to 32 samples.");
    }
    else if (_fragm > 4096)
    {
        logger_.log(Logger::WARNING, "JACK buffer size is too large. Setting it to 4096 samples.");
    }

    pthread_getschedparam(jack_client_thread_id(_jack_client), &_policy, &spar);
    _abspri = spar.sched_priority;

    memset(_jack_outpp, 0, Convproc::MAXOUT * sizeof(jack_port_t*));
    _prbsgen.set_poly(Prbsgen::G32);

    return true;
}

void Jclient::close_jack()
{
    if (jack_deactivate(_jack_client))
        logger_.log(Logger::ERROR, "Unable to deactivate the jack client");
    if (jack_client_close(_jack_client))
        logger_.log(Logger::ERROR, "Unable to close the jack client");
}

void Jclient::jack_static_shutdown(void* arg)
{
    Jclient* C = (Jclient*)arg;

    C->_flags = FL_EXIT;
}

void Jclient::jack_static_freewheel(int state, void* arg)
{
    Jclient* C = (Jclient*)arg;

    C->_convsync = state ? true : false;
}

int Jclient::jack_static_buffsize(jack_nframes_t nframes, void* arg)
{
    Jclient* C = (Jclient*)arg;

    if (C->_fragm)
    {
        C->stop();
        C->_flags = FL_BUFF;
    }
    return 0;
}

#ifdef __CONV_TEST__
// This function bypasses jack audio callbacks and
// enables direct supply of samples to the convolver.
void Jclient::jack_process(float* inbuf, float* outbuf)
{
    unsigned int i;

    if (_convproc->state() == Convproc::ST_WAIT)
        _convproc->check_stop();

    if (_convproc->state() != Convproc::ST_PROC)
    {
        memset(outbuf, 0, _fragm * sizeof(float));
        return;
    }

    auto q = _convproc->inpdata(_activeInputPorts.begin()->first);
    if (!q)
        return;
    memcpy(q, inbuf, _fragm * sizeof(float));

    _flags |= _convproc->process(_convsync);
    for (i = 0; i < _nout; i++)
    {
        memcpy(outbuf, _convproc->outdata(i), _fragm * sizeof(float));
    }
}
#else
int Jclient::jack_static_process(jack_nframes_t nframes, void* arg)
{
    Jclient* C = (Jclient*)arg;

    C->jack_process();
    return 0;
}

void Jclient::jack_process(void)
{
    unsigned int i, j;
    float* outp[Convproc::MAXOUT];

    for (i = 0; i < _nout; i++)
    {
        outp[i] = (float*)jack_port_get_buffer(_jack_outpp[i], _fragm);
    }

    if (_clear)
    {
        _flags = 0;
        _clear = false;
    }

    if (_convproc->state() == Convproc::ST_WAIT)
        _convproc->check_stop();

    if (_convproc->state() != Convproc::ST_PROC)
    {
        for (i = 0; i < _nout; i++)
            memset(outp[i], 0, _fragm * sizeof(float));
        return;
    }

    for (const auto& it : _activeInputPorts)
    {
        auto p = static_cast<float*>(jack_port_get_buffer(it.second, _fragm));
        auto q = _convproc->inpdata(it.first);
        if (!q)
            continue;

        if (_addnoise)
        {
            for (j = 0; j < _fragm; j++)
            {
                q[j] = p[j] + 1e-15f * (0.5f - _prbsgen.step());
            }
        }
        else
        {
            memcpy(q, p, _fragm * sizeof(float));
        }
        if (_dopplerEngine)
            _dopplerEngine->loadInputBlock(it.first, q, _fragm);
    }
    _flags |= _convproc->process(_convsync);
    if (_dopplerEngine)
        _dopplerEngine->process();
    for (i = 0; i < _nout; i++)
    {
        memcpy(outp[i], _convproc->outdata(i), _fragm * sizeof(float));
        if (_dopplerEngine)
        {
            // add samples from doppler engine
            auto dopplerOut = _dopplerEngine->getOutputBlock(i);
            for (size_t samp = 0; samp < _fragm; ++samp)
                outp[i][samp] += dopplerOut[samp];
        }
    }
}
#endif

int Jclient::add_input_port(const std::string& name, const std::string& conn)
{
    if (_ninp == Convproc::MAXINP)
        return -1;
    jack_port_t* jackPort = jack_port_register(_jack_client, name.c_str(), JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
    if (!jackPort) // Error creating jack port
    {
        logger_.log(Logger::ERROR, "Unable to create JACK input named " + name);
        return -1;
    }
    if (!conn.empty())
    {
        jack_connect(_jack_client, conn.c_str(), (_jname + ":" + name).c_str());
    }
    _activeInputPorts.insert(std::make_pair(std::string(name), jackPort));
    return _ninp++;
}

int Jclient::add_output_port(const std::string& name, const std::string& conn)
{
    if ((_convproc->state() > Convproc::ST_STOP) || (_nout == Convproc::MAXOUT))
    {
        logger_.log(Logger::ERROR, "Cannot create JACK output ports. Too many output ports or the convolver is active.");
        return -1;
    }

    _jack_outpp[_nout] = jack_port_register(_jack_client, name.c_str(), JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
    if (!_jack_outpp[_nout])
    {
        logger_.log(Logger::ERROR, "Unable to create JACK output");
        return -1;
    }
    if (!conn.empty())
    {
        jack_connect(_jack_client, (_jname + ":" + name).c_str(), conn.c_str());
    }
    return _nout++;
}

bool Jclient::is_input_port(const std::string& portName)
{
    auto it = _activeInputPorts.find(portName);
    if (it != _activeInputPorts.end())
        return true;
    else
        return false;
}