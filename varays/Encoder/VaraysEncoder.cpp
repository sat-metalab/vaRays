// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <algorithm>
#include <cstring>
#include <numeric>

#include <Eigen/Dense>
#include <Eigen/StdVector>
#include <sndfile.h>

#include "../Utils/Matrix3d.hpp"
#include "../Utils/Matrix4d.hpp"
#include "./Ambisonics.hpp"
#include "./VaraysEncoder.hpp"

namespace varays
{
VaraysEncoder::VaraysEncoder(const std::vector<float>& frequencyBands, int sampleRate)
    : sampleRate_(sampleRate)
{
    irFilter_ = std::make_unique<LRTree>(frequencyBands, sampleRate_);
    initializeAirAttenuations(irFilter_->getCentreFrequencies());
}

void VaraysEncoder::setAtmosphericConditions(float humidity, float pressure, double temperature)
{
    humidity_ = humidity;
    pressure_ = pressure;
    temperature_ = temperature;
    updateSpeedOfSound();
    initializeAirAttenuations(irFilter_->getCentreFrequencies());
}

void VaraysEncoder::setSampleRate(int sampleRate)
{
    if (sampleRate != sampleRate_)
    {
        sampleRate_ = sampleRate;
        auto freqBands = irFilter_->getCentreFrequencies();
        irFilter_ = std::make_unique<LRTree>(freqBands, sampleRate_);
    }
}

void VaraysEncoder::initializeAirAttenuations(const std::vector<float>& frequencies)
{
    airAttenuations_.clear();
    for (const auto& f : frequencies)
        airAttenuations_.push_back(getAbsorptionForFrequency(f));
}

Eigen::ArrayXd VaraysEncoder::getAirAttenuation(float length)
{
    Eigen::Map<Eigen::ArrayXd> attByFreq(airAttenuations_.data(), airAttenuations_.size());
    return Eigen::pow(10.0, -attByFreq * length / 20.0);
}

// [Source][ir]
std::map<unsigned int, std::vector<float>> VaraysEncoder::writeIR(int ambiOrder, const std::vector<SoundEvent>& soundEvents, bool includeDirectSound)
{
    logger_.log(Logger::DEBUG, "Rays to sources: " + std::to_string(soundEvents.size()));

    std::map<unsigned int, std::vector<SoundEvent>> sources;
    std::map<unsigned int, int> maxPathSamples;
    for (const auto& soundEvent : soundEvents)
    {
        auto it = sources.find(soundEvent.sourceID_);
        if (it == sources.end())
        {
            sources.insert(std::make_pair(soundEvent.sourceID_, std::vector<SoundEvent>()));
            maxPathSamples.insert(std::make_pair(soundEvent.sourceID_, soundEvent.getFrameForSamplingRate(sampleRate_, speedOfSound_)));
        }
        if (!includeDirectSound && soundEvent.getReflectionTypes().empty())
            continue;
        sources[soundEvent.sourceID_].push_back(soundEvent);
        maxPathSamples[soundEvent.sourceID_] = std::max(maxPathSamples[soundEvent.sourceID_], soundEvent.getFrameForSamplingRate(sampleRate_, speedOfSound_));
    }

    const int channels = pow(ambiOrder + 1, 2);
    std::map<unsigned int, std::vector<float>> ir;
    for (auto& src : sources)
    {
        // Generate a 2D array containing impulses for paths arriving at the listener from the source
        std::vector<Eigen::Array<double, -1, -1, Eigen::RowMajor>, Eigen::aligned_allocator<Eigen::Array<double, -1, -1, Eigen::RowMajor>>> impulseTrain(
            channels);
        int numSamples = maxPathSamples[src.first] + 512;
        for (auto& it : impulseTrain)
        {
            it.resize(irFilter_->getNumInputs(), numSamples);
            it.setZero();
        }

        for (const auto& sndEvent : src.second)
        {
            int sampArrival = sndEvent.getFrameForSamplingRate(sampleRate_, speedOfSound_);

            Eigen::ArrayXd pathGain(irFilter_->getNumInputs());
            const Eigen::Map<const Eigen::ArrayXf> bandEnergies(sndEvent.energies_.data(), sndEvent.energies_.size());
            pathGain = bandEnergies.sqrt().cast<double>() * getAirAttenuation(sndEvent.length_);

            std::vector<double> ambiGain = HOA::getAmbi(sndEvent.sphericalCoordinates_[1], sndEvent.sphericalCoordinates_[2], ambiOrder);
            for (int ch = 0; ch < channels; ++ch)
                impulseTrain[ch].col(sampArrival) += ambiGain[ch] * pathGain;
        }

        ir.insert(std::make_pair(src.first, std::vector<float>(numSamples * channels)));
        for (auto& it : impulseTrain)
        {
            irFilter_->resetFilters();
            irFilter_->processBlock(it);
            for (int ch = 0; ch < channels; ++ch)
            {
                for (int samp = 0; samp < numSamples; ++samp)
                    ir[src.first][samp * channels + ch] = impulseTrain[ch](0, samp);
            }
        }
    }

    return ir;
}

// Currently interpolates linearly (crossfade) between multiple soundevent vectors
std::vector<SoundEvent> VaraysEncoder::interpolateSoundEvents(const std::vector<WeightedSoundEvents>& soundEvents)
{
    float sumWeights =
        std::accumulate(soundEvents.begin(), soundEvents.end(), 0.f, [](const auto& value, const auto& soundEvent) { return value + soundEvent.weight_; });
    std::vector<SoundEvent> result;
    for (const auto& weightAndEvents : soundEvents)
    {
        float ratio = weightAndEvents.weight_ / sumWeights;
        for (const auto& event : weightAndEvents.soundEvents_)
        {
            result.emplace_back(event);
            std::transform(
                event.energies_.begin(), event.energies_.end(), result.back().energies_.begin(), [ratio](const auto& energy) { return energy * ratio; });
        }
    }
    return result;
}

void VaraysEncoder::updateSpeedOfSound()
{
    double const Rd = 287.058; // specific gas constant for dry air
    double const Rv = 461.495; // specific gas constant for water vapor

    double T = temperature_ + 273.15; // temperature in kelvin
    double pv = 6.1078 * pow(10.0, (7.5 / T) / (T + 237.3)) * humidity_ / 100.0;
    double pd = pressure_ * 1000.0 - pv; // pressure in pascal

    double rho = pd / (Rd * (T)) + pv / (Rv * (T)); // density of the air

    speedOfSound_ = sqrt(1.4 * (pressure_ * 1000.0) / rho);
}

double VaraysEncoder::getAbsorptionForFrequency(float frequency)
{
    // initial sound pressure at r=0 (path length of traveling wave) is considered to be 1 atm
    double T = temperature_ + 273.15; // atmospheric temperature (K)

    double ps = pressure_ / 101.325; // atmospheric pressure

    // (ps0 * pow(10.0, -6.8346 * pow(273.16 / T, 1.261) + 4.6151)) = saturated vapor pressure
    double h = (humidity_ / ps) * ((pow(10.0, -6.8346 * pow(273.16 / T, 1.261) + 4.6151)));

    double Fr0 = 1.0 * (24.0 + 4.04 * pow(10.0, 4.0) * h * (0.02 + h) / (0.391 + h));

    double T0 = 293.15; // reference temperature
    double FrN = (1.0 * sqrt(T0 / T) * (9.0 + 280.0 * h * exp(-4.17 * (pow(T0 / T, 1.0 / 3.0) - 1.0))));

    double F = frequency / ps;
    // see https://en.wikibooks.org/wiki/Engineering_Acoustics/Outdoor_Sound_Propagation (Attenuation by atmospheric absorption)
    return (20.0 / log(10.0)) * (pow(F, 2)) *
           (1.84 * pow(10.0, -11.0) * sqrt(T / T0) +
               pow(T / T0, -5.0 / 2.0) * (0.01275 * (exp(-2239.1 / T) / (Fr0 + pow(F, 2.0) / Fr0)) + 0.1068 * (exp(-3352.0 / T) / (FrN + pow(F, 2.0) / FrN)))) *
           ps;
}

void VaraysEncoder::exportIR(const std::vector<float>& buffer, const std::string& path, int channels, int sampleRate)
{
    SF_INFO sfinfo;
    SNDFILE* outfile;

    memset(&sfinfo, 0, sizeof(sfinfo));

    // Saving the frames in a temporary variable is necessary because sf_open will set the sfinfo.frames value to 0
    int64_t frames = std::floor(buffer.size() / channels);
    sfinfo.frames = frames;
    sfinfo.samplerate = sampleRate;
    sfinfo.channels = channels;
    sfinfo.format = SF_FORMAT_WAV | SF_FORMAT_PCM_16; // SF_FORMAT_FLOAT; For some reason can't open
                                                      // file if specified with a sndfile macro
    sfinfo.sections = 1;
    sfinfo.seekable = 1;

    if ((outfile = sf_open((path + ".wav").c_str(), SFM_WRITE, &sfinfo)) == nullptr)
    {
        logger_.log(Logger::ERROR, "Unable to export IR");
        return;
    }

    sf_writef_float(outfile, buffer.data(), frames);
    sf_close(outfile);
}
}; // namespace varays
