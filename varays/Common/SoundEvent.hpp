// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef __VARAYS_SOUND_EVENT__
#define __VARAYS_SOUND_EVENT__

#include <math.h>
#include <string>
#include <vector>

#include <Eigen/Dense>

#include "../Utils/JsonSave.hpp"

class SoundEvent : public JsonSerializable
{
  public:
    enum Reflection
    {
        SPECULAR = 0,
        DIFFUSION = 1,
        DIFFRACTION = 2,
        TRANSMISSION = 3
    };

    struct SoundEventConfig
    {
        SoundEventConfig(unsigned int sourceID,
            float length,
            const std::vector<float>& frequencies,
            const std::vector<float>& energies,
            const Eigen::Vector3f& sphericalCoordinates,
            const std::vector<Reflection>& reflections)
            : sourceID(sourceID)
            , length(length)
            , frequencies(frequencies)
            , energies(energies)
            , sphericalCoordinates(sphericalCoordinates)
            , reflections(reflections){};

        SoundEventConfig();

        unsigned int sourceID = 0;
        float length = 0;
        std::vector<float> frequencies = std::vector<float>();
        const std::vector<float> energies = std::vector<float>();
        const Eigen::Vector3f sphericalCoordinates = Eigen::Vector3f();
        const std::vector<Reflection> reflections = std::vector<Reflection>();
    };

    SoundEvent() = default;
    SoundEvent(SoundEventConfig config)
        : sourceID_(config.sourceID)
        , length_(config.length)
        , frequencies_(config.frequencies)
        , energies_(config.energies)
        , sphericalCoordinates_(config.sphericalCoordinates)
        , reflections_(config.reflections){};

    // Using linear interpolation between the calculated frequencies
    std::vector<float> getEnergies() const { return energies_; };
    float getEnergyForFrequency(float frequency) const;

    // returns a float between 0 and 2*pi
    float getPhaseForFrequency(float frequency, float speedOfSound) const;

    int getFrameForSamplingRate(int SamplingRate, float speedOfSound) const { return round((length_ / speedOfSound) * SamplingRate); }

    std::vector<Reflection> getReflectionTypes() const { return reflections_; }

    // Functions for serializing and deserializing using jsoncpp
    void serialize(Json::Value& root) const;
    void deserialize(const Json::Value& root);
    std::string type_string() const // for identifying class name in json file
    {
        return "SoundEvent";
    }

    // For comparing two SoundEvent objects (used for testing)
    bool operator==(const SoundEvent& b) const;

    unsigned int sourceID_;
    float length_;
    std::vector<float> frequencies_;
    std::vector<float> energies_;
    Eigen::Vector3f sphericalCoordinates_;
    std::vector<Reflection> reflections_;
};
#endif
