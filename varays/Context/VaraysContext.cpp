// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <algorithm>
#include <fstream>

#include <jsoncpp/json/json.h>

#include "./VaraysContext.hpp"

using namespace std;

namespace varays
{

const float VaraysContext::DiffractionThreshold = 0.7f;
const float VaraysContext::PositionUpdateThreshold = 0.01f;

VaraysContext::VaraysContext(const std::string& materialFilePath, const Eigen::Vector3f listenerPos, bool cacheEnabled, const vector<AudioSource> sources)
    : listener_(listenerPos)
    , cacheEnabled_(cacheEnabled)
{
    initialize(sources);
    if (!materialFilePath.empty())
        loadMaterialsFromFile(materialFilePath);
}

VaraysContext::~VaraysContext()
{
    rtcReleaseScene(scene_);
    rtcReleaseDevice(device_);
}

void VaraysContext::initialize(vector<AudioSource> sources)
{
    device_ = rtcNewDevice(nullptr);
    scene_ = rtcNewScene(device_);
    rtcSetSceneFlags(scene_, RTC_SCENE_FLAG_DYNAMIC);
    for (const AudioSource& source : sources)
    {
        addSource(source);
    }
    loadDefaultMat();
}

unsigned int VaraysContext::addGeom(const vector<Eigen::Vector3f>& vertices, const vector<int>& indices, bool diffraction, const string& matName)
{
    RTCGeometry geometry = rtcNewGeometry(device_, RTC_GEOMETRY_TYPE_TRIANGLE);
    unsigned int geomID = rtcAttachGeometry(scene_, geometry);
    auto geomVertices = static_cast<Eigen::Vector3f*>(
        rtcSetNewGeometryBuffer(geometry, RTC_BUFFER_TYPE_VERTEX, 0, RTC_FORMAT_FLOAT3, sizeof(Eigen::Vector3f), vertices.size()));
    for (unsigned int i = 0; i < vertices.size(); ++i)
    {
        geomVertices[i] = vertices[i];
    }

    Triangle* triangles =
        static_cast<Triangle*>(rtcSetNewGeometryBuffer(geometry, RTC_BUFFER_TYPE_INDEX, 0, RTC_FORMAT_UINT3, sizeof(Triangle), indices.size() / 3));

    for (unsigned int tri = 0; tri < indices.size() / 3; ++tri)
    {
        triangles[tri].v0 = indices[3 * tri];
        triangles[tri].v1 = indices[3 * tri + 1];
        triangles[tri].v2 = indices[3 * tri + 2];
    }

    rtcSetGeometryVertexAttributeCount(geometry, 1);

    auto itMat = materials_.find(matName);
    if (itMat == materials_.end())
    {
        logger_.log(Logger::WARNING, "Material " + matName + " not found, default material used instead");
        itMat = materials_.find("Default");
    }

    rtcCommitGeometry(geometry);

    unique_ptr<GeomData> dataPtr = make_unique<GeomData>(vertices.size(), &itMat->second);
    rtcSetGeometryUserData(geometry, dataPtr.get());
    geomDatas_.push_back(move(dataPtr));

    if (diffraction)
    {
        for (unsigned int tri = 0; tri < indices.size() / 3; ++tri)
        {
            vector<Eigen::Vector3f> cmnPnt = vector<Eigen::Vector3f>();
            vector<Eigen::Vector3f> difPnt = vector<Eigen::Vector3f>();
            for (unsigned i = 0; i < tri; ++i)
            {
                bool isDifferent[3] = {true, true, true};
                for (int j = 0; j < 3; ++j)
                {
                    if (indices[3 * tri + j] == indices[3 * i])
                    {
                        cmnPnt.push_back(vertices[indices[3 * tri + j]]);
                        isDifferent[0] = false;
                    }
                    else if (indices[3 * tri + j] == indices[3 * i + 1])
                    {
                        cmnPnt.push_back(vertices[indices[3 * tri + j]]);
                        isDifferent[1] = false;
                    }
                    else if (indices[3 * tri + j] == indices[3 * i + 2])
                    {
                        cmnPnt.push_back(vertices[indices[3 * tri + j]]);
                        isDifferent[2] = false;
                    }
                    else
                    {
                        difPnt.push_back(vertices[indices[3 * tri + j]]);
                    }
                }
                if (cmnPnt.size() == 2)
                {
                    for (int j = 0; j < 3; ++j)
                    {
                        if (isDifferent[j])
                        {
                            difPnt.push_back(vertices[indices[3 * i + j]]);
                        }
                    }
                    Eigen::Vector3f v1 = (cmnPnt[1] - cmnPnt[0]).cross(difPnt[0] - cmnPnt[0]).normalized();
                    Eigen::Vector3f v2 = (difPnt[1] - cmnPnt[0]).cross(cmnPnt[1] - cmnPnt[0]).normalized();
                    if (v1.dot(v2) < DiffractionThreshold)
                    {
                        addDiffraction(cmnPnt[0], cmnPnt[1], geomID, scene_, matName);
                    }
                }
                cmnPnt.clear();
                difPnt.clear();
            }
        }
    }

    rtcReleaseGeometry(geometry);
    return geomID;
}

unsigned int VaraysContext::addModel(const vector<Eigen::Vector3f>& vertices,
    const vector<int>& indices,
    unsigned int modelKey,
    bool diffraction,
    bool instantiate,
    const TransformationMatrix& mat,
    const string& matName)
{
    RTCScene model = rtcNewScene(device_);
    RTCGeometry geometry = rtcNewGeometry(device_, RTC_GEOMETRY_TYPE_TRIANGLE);
    auto geomVertices = static_cast<Eigen::Vector3f*>(
        rtcSetNewGeometryBuffer(geometry, RTC_BUFFER_TYPE_VERTEX, 0, RTC_FORMAT_FLOAT3, sizeof(Eigen::Vector3f), vertices.size()));
    for (unsigned int i = 0; i < vertices.size(); ++i)
    {
        geomVertices[i] = vertices[i];
    }

    Triangle* triangles =
        static_cast<Triangle*>(rtcSetNewGeometryBuffer(geometry, RTC_BUFFER_TYPE_INDEX, 0, RTC_FORMAT_UINT3, sizeof(Triangle), indices.size() / 3));

    for (unsigned int tri = 0; tri < indices.size() / 3; ++tri)
    {
        triangles[tri].v0 = indices[3 * tri];
        triangles[tri].v1 = indices[3 * tri + 1];
        triangles[tri].v2 = indices[3 * tri + 2];
    }

    rtcSetGeometryVertexAttributeCount(geometry, 1);

    auto itMat = materials_.find(matName);
    if (itMat == materials_.end())
    {
        logger_.log(Logger::WARNING, "Material " + matName + " not found, default material used instead");
        itMat = materials_.find("Default");
    }
    rtcCommitGeometry(geometry);
    unsigned int geomID = rtcAttachGeometry(scene_, geometry);

    unique_ptr<GeomData> dataPtr = make_unique<GeomData>(vertices.size(), &itMat->second);
    rtcSetGeometryUserData(geometry, dataPtr.get());
    geomDatas_.push_back(move(dataPtr));

    if (diffraction)
    {
        for (unsigned int tri = 0; tri < indices.size() / 3; ++tri)
        {
            vector<Eigen::Vector3f> cmnPnt = vector<Eigen::Vector3f>();
            vector<Eigen::Vector3f> difPnt = vector<Eigen::Vector3f>();
            for (unsigned i = 0; i < tri; ++i)
            {
                bool isDifferent[3] = {true, true, true};
                for (int j = 0; j < 3; ++j)
                {
                    if (indices[3 * tri + j] == indices[3 * i])
                    {
                        cmnPnt.push_back(vertices[indices[3 * tri + j]]);
                        isDifferent[0] = false;
                    }
                    else if (indices[3 * tri + j] == indices[3 * i + 1])
                    {
                        cmnPnt.push_back(vertices[indices[3 * tri + j]]);
                        isDifferent[1] = false;
                    }
                    else if (indices[3 * tri + j] == indices[3 * i + 2])
                    {
                        cmnPnt.push_back(vertices[indices[3 * tri + j]]);
                        isDifferent[2] = false;
                    }
                    else
                    {
                        difPnt.push_back(vertices[indices[3 * tri + j]]);
                    }
                }
                if (cmnPnt.size() == 2)
                {
                    for (int j = 0; j < 3; ++j)
                    {
                        if (isDifferent[j])
                        {
                            difPnt.push_back(vertices[indices[3 * i + j]]);
                        }
                    }
                    Eigen::Vector3f v1 = (cmnPnt[1] - cmnPnt[0]).cross(difPnt[0] - cmnPnt[0]).normalized();
                    Eigen::Vector3f v2 = (difPnt[1] - cmnPnt[0]).cross(cmnPnt[1] - cmnPnt[0]).normalized();
                    if (v1.dot(v2) < DiffractionThreshold)
                    {
                        addDiffraction(cmnPnt[0], cmnPnt[1], geomID, model, matName);
                    }
                }
                cmnPnt.clear();
                difPnt.clear();
            }
        }
    }
    rtcReleaseGeometry(geometry);
    rtcCommitScene(model);
    models_.emplace(modelKey, model);
    if (instantiate)
    {
        return instantiateModel(modelKey, mat);
    }
    else
    {
        return -1;
    }
}

unsigned int VaraysContext::instantiateModel(unsigned int modelKey, const TransformationMatrix& mat)
{
    RTCGeometry instance = rtcNewGeometry(device_, RTC_GEOMETRY_TYPE_INSTANCE);
    rtcSetGeometryInstancedScene(instance, models_[modelKey]);
    rtcSetGeometryTimeStepCount(instance, 1);
    unsigned int sceneId = rtcAttachGeometry(scene_, instance);
    rtcReleaseGeometry(instance);
    setTransform(sceneId, mat);
    rtcCommitScene(scene_);
    return sceneId;
}

void VaraysContext::setTransform(unsigned int sceneId, const TransformationMatrix& mat)
{
    float tArray[4][3];
    for (int i = 0; i < 4; ++i)
    {
        tArray[i][0] = mat[i].x();
        tArray[i][1] = mat[i].y();
        tArray[i][2] = mat[i].z();
    }
    RTCGeometry instance = rtcGetGeometry(scene_, sceneId);
    rtcSetGeometryTransform(instance, 0, RTC_FORMAT_FLOAT3X4_COLUMN_MAJOR, tArray);
    rtcCommitGeometry(instance);
    rtcCommitScene(scene_);
}

bool VaraysContext::readObjFile(const string& path, bool diffraction)
{
    objl::Loader loader;
    logger_.log(Logger::INFO, "Loading 3D scene from " + path);
    if (!loader.LoadFile(path))
    {
        logger_.log(Logger::ERROR, "Unable to read " + path);
        return false;
    }
    uint32_t totalVertices = 0;
    for (auto& mesh : loader.LoadedMeshes)
    {
        logger_.log(Logger::DEBUG, "Adding a geom");
        // Necessary because addGeom needs the indices to start at 0
        for (auto& indic : mesh.Indices)
        {
            indic -= totalVertices;
        }
        totalVertices += mesh.Vertices.size();
        // Geom has a material
        if (!mesh.MeshMaterial.name.empty() && mesh.MeshMaterial.name != "None")
            addGeom(mesh.Vertices, mesh.Indices, diffraction, mesh.MeshMaterial.name);
        // Geom doesn't have a material (use default one)
        else
            addGeom(mesh.Vertices, mesh.Indices, diffraction);
    }
    rtcCommitScene(scene_);
    logger_.log(Logger::INFO, "Finished loading scene");
    return true;
}

bool VaraysContext::loadMat(const string& name, const map<float, float>& absorption, const map<float, float>& transmission, const map<float, float>& scattering)
{
    AudioMat newMat(absorption, transmission, scattering);
    materials_.emplace(name, newMat);
    return true;
}

void VaraysContext::addDiffraction(
    const Eigen::Vector3f point1, const Eigen::Vector3f& point2, unsigned int parentID, RTCScene scene, const string& matName, float size)
{
    RTCGeometry mesh = rtcNewGeometry(device_, RTC_GEOMETRY_TYPE_TRIANGLE);
    Eigen::Vector3f v = (point2 - point1).normalized();
    Eigen::Vector3f v1 = Eigen::Vector3f(1.0f, .0f, .0f);
    Eigen::Vector3f v2 = Eigen::Vector3f(.0f, 1.0f, .0f);
    float temp = v1.dot(v);
    if (abs(1.f - abs(temp)) < 0.000001f)
    {
        v1 = Eigen::Vector3f(.0f, .0f, 1.0f);
    }
    else
    {
        v1 = (v1 - v * temp).normalized();
        temp = v2.dot(v);
        if (abs(1.f - abs(temp)) < 0.000001f)
        {
            v2 = Eigen::Vector3f(.0f, .0f, 1.0f);
        }
        else
        {
            v2 = v2 - v * temp;
            v2 = (v2 - v1 * v2.dot(v1)).normalized();
        }
    }

    auto vertices = static_cast<Eigen::Vector3f*>(rtcSetNewGeometryBuffer(mesh, RTC_BUFFER_TYPE_VERTEX, 0, RTC_FORMAT_FLOAT3, sizeof(Eigen::Vector3f), 8));

    vertices[0] = point1 + size * (v1 + v2 - v);
    vertices[1] = point1 + size * (v1 - v2 - v);
    vertices[2] = point1 + size * (-v1 + v2 - v);
    vertices[3] = point1 + size * (-v1 - v2 - v);
    vertices[4] = point2 + size * (v1 + v2 + v);
    vertices[5] = point2 + size * (v1 - v2 + v);
    vertices[6] = point2 + size * (-v1 + v2 + v);
    vertices[7] = point2 + size * (-v1 - v2 + v);

    Triangle* triangles = static_cast<Triangle*>(rtcSetNewGeometryBuffer(mesh, RTC_BUFFER_TYPE_INDEX, 0, RTC_FORMAT_UINT3, sizeof(Triangle), 12));

    vector<int> indices{0, 1, 2, 1, 3, 2, 4, 6, 5, 5, 6, 7, 0, 4, 1, 1, 4, 5, 2, 3, 6, 3, 7, 6, 0, 2, 4, 2, 6, 4, 1, 5, 3, 3, 5, 7};

    for (int tri = 0; tri < 12; ++tri)
    {
        triangles[tri].v0 = indices[3 * tri];
        triangles[tri].v1 = indices[3 * tri + 1];
        triangles[tri].v2 = indices[3 * tri + 2];
    }
    rtcSetGeometryVertexAttributeCount(mesh, 1);

    auto itMat = materials_.find(matName);
    if (itMat == materials_.end())
    {
        logger_.log(Logger::WARNING, "Material " + matName + " not found, default material used instead");
        itMat = materials_.find("Default");
    }

    rtcCommitGeometry(mesh);
    unsigned int geomID = rtcAttachGeometry(scene_, mesh);

    unique_ptr<GeomData> dataPtr = make_unique<DiffractionData>(8, &itMat->second, geomID, parentID, point1, point2, size);
    rtcSetGeometryUserData(mesh, dataPtr.get());
    geomDatas_.push_back(move(dataPtr));

    rtcReleaseGeometry(mesh);
    rtcCommitScene(scene);
    logger_.log(Logger::DEBUG, "Added difraction");
}

unsigned int VaraysContext::getSourceIDFromName(const std::string& name)
{
    auto it = find_if(sources_.begin(), sources_.end(), [&](const auto& src) { return src.getName() == name; });
    if (it != sources_.end())
        return it->getId();

    logger_.log(Logger::WARNING, "Source " + name + " not found, return 0");
    return 0;
}

unsigned int VaraysContext::addSource(const Eigen::Vector3f& srcPos, const string& name)
{
    for (auto& src : sources_)
    {
        if (src.getName() == name)
        {
            logger_.log(Logger::WARNING, "Source " + name + " already exists, stored datas will be lost!");
            src.setPos(srcPos);
            return src.getId();
        }
    }
    AudioSource newSource(++sourceIDCounter_, name, srcPos, frequencies_, energyFromRays_);
    sources_.emplace_back(newSource);
    logger_.log(
        Logger::INFO, "New source: " + name + " at " + std::to_string(srcPos.x()) + ", " + std::to_string(srcPos.y()) + ", " + std::to_string(srcPos.z()));
    return sourceIDCounter_;
}

bool VaraysContext::addSource(const AudioSource& newSource)
{
    if (find_if(sources_.begin(), sources_.end(), [&](const auto& a) {
            return (a.getId() == newSource.getId() || (a.getName() != "" && a.getName() == newSource.getName()));
        }) != sources_.end())
    {
        logger_.log(Logger::WARNING, "Source " + newSource.getName() + " already exists");
        return false;
    }
    else
    {
        Eigen::Vector3f srcPos = newSource.getPos();
        sources_.push_back(newSource);
        logger_.log(Logger::INFO,
            "New source: " + newSource.getName() + " at " + std::to_string(srcPos.x()) + ", " + std::to_string(srcPos.y()) + ", " + std::to_string(srcPos.z()));
        return true;
    }
}

void VaraysContext::moveListener(const Eigen::Vector3f& newPos)
{
    listener_ = newPos;
}

bool VaraysContext::moveSource(unsigned int id, const Eigen::Vector3f& newPos)
{
    auto it = find_if(sources_.begin(), sources_.end(), [&](const auto& a) { return (a.getId() == id); });
    if (it != sources_.end())
    {
        it->setPos(newPos);
        return true;
    }
    else
    {
        logger_.log(Logger::Priority::WARNING, "Moving source failed - could not find source with id : " + to_string(id));
        return false;
    }
}

bool VaraysContext::removeSource(unsigned int id)
{
    auto it = find_if(sources_.begin(), sources_.end(), [&](const auto& a) { return (a.getId() == id); });
    if (it != sources_.end())
    {
        logger_.log(Logger::INFO, "Removed " + it->getName());
        sources_.erase(it);
        return true;
    }
    else
    {
        return false;
    }
}

AudioSource VaraysContext::getSource(unsigned int i) const
{
    auto it = find_if(sources_.begin(), sources_.end(), [&](const auto& src) { return src.getId() == i; });
    if (it == sources_.end())
    {
        logger_.log(Logger::WARNING, "Source not found");
        return AudioSource(-1, "INVALID", {0, 0, 0}, {}, 0);
    }
    return *it;
}

void VaraysContext::setFrequencies(const vector<float>& frequencies)
{
    frequencies_ = frequencies;
}

void VaraysContext::writeRaysToObj(const std::string& path)
{
    rayLines_.clear();
    ofstream ofs;
    ofs.open(path + exportObjFileName, ofstream::out | ofstream::trunc);
    string coords;
    for (size_t counter = 0; counter < rayLines_.size(); ++counter)
    {
        coords.append(
            "v " + to_string(rayLines_[counter][0][0]) + " " + to_string(rayLines_[counter][0][1]) + " " + to_string(rayLines_[counter][0][2]) + "\n");
        coords.append(
            "v " + to_string(rayLines_[counter][1][0]) + " " + to_string(rayLines_[counter][1][1]) + " " + to_string(rayLines_[counter][1][2]) + "\n");
    }
    for (size_t counter = 0; counter < rayLines_.size(); ++counter)
    {
        coords.append("l " + to_string(counter * 2 + 1) + " " + to_string(counter * 2 + 2) + "\n");
    }
    ofs << coords;
    ofs.close();
}

void VaraysContext::loadDefaultMat()
{
    string matName("Default");
    const map<float, float> absorption{{125.f, 0.1f}, {250.f, 0.1f}, {500.f, 0.1f}, {1000.f, 0.1f}, {2000.f, 0.1f}, {4000.f, 0.1f}, {8000.f, 0.1f}, {16000.f, 0.1f}};
    const map<float, float> transmission{
        {125.f, 0.0f}, {250.f, 0.0f}, {500.f, 0.0f}, {1000.f, 0.0f}, {2000.f, 0.0f}, {4000.f, 0.0f}, {8000.f, 0.0f}, {16000.f, 0.0f}};
    const map<float, float> scattering{{125.f, 0.1f}, {250.f, 0.2f}, {500.f, 0.3f}, {1000.f, 0.4f}, {2000.f, 0.5f}, {4000.f, 0.6f}, {8000.f, 0.7f}, {16000.f, 0.8f}};
    loadMat(matName, absorption, transmission, scattering);
}

void VaraysContext::loadMaterialsFromFile(const std::string& matFilePath)
{
    logger_.log(Logger::INFO, "Loading material properties from " + matFilePath);

    // Load JSON file
    std::ifstream ifile;
    ifile.open(matFilePath);
    std::string jsonFile((std::istreambuf_iterator<char>(ifile)), (std::istreambuf_iterator<char>()));
    ifile.close();

    Json::Value deserializeRoot;
    Json::Reader reader;

    if (!reader.parse(jsonFile, deserializeRoot))
    {
        logger_.log(Logger::ERROR, "Unable to load materials from " + matFilePath + ". Using default values.");
        return;
    }

    // Read frequency bands for which material properties are given
    if (!deserializeRoot.isMember("FrequencyBands"))
    {
        logger_.log(Logger::ERROR, "No field named \"FrequencyBands\".");
        return;
    }

    auto fBands_json = deserializeRoot.get("FrequencyBands", 0);
    if (!fBands_json.isArray())
    {
        logger_.log(Logger::ERROR, "Incorrect format. \"FrequencyBands\" must be an array.");
        return;
    }

    frequencies_.clear();
    for (const auto& fb : deserializeRoot.get("FrequencyBands", 0))
        frequencies_.push_back(fb.asFloat());

    // Load material properties
    if (!deserializeRoot.isMember("Materials"))
    {
        logger_.log(Logger::ERROR, "No field named \"Materials\".");
        return;
    }

    auto mat_json = deserializeRoot.get("Materials", 0);
    if (mat_json == 0)
    {
        logger_.log(Logger::ERROR, "No materials listed.");
        return;
    }

    int unknownMatInd = 0;
    for (const auto& m : mat_json)
    {
        std::string matName = m.get("Name", "unknown_material_" + unknownMatInd++).asString();
        std::transform(matName.begin(), matName.end(), matName.begin(), ::tolower);
        std::map<float, float> abs_map, trans_map, scat_map;
        Json::Value abs_json, trans_json, scat_json;
        if (m.isMember("Absorption"))
            abs_json = m.get("Absorption", 0);
        if (m.isMember("Transmission"))
            trans_json = m.get("Transmission", 0);
        if (m.isMember("Scattering"))
            scat_json = m.get("Scattering", 0);

        for (size_t i = 0; i < frequencies_.size(); ++i)
        {
            float abs = abs_json.get(static_cast<Json::ArrayIndex>(i), (abs_json.empty() ? 0.0f : abs_json[abs_json.size() - 1].asFloat())).asFloat();
            float trans = trans_json.get(static_cast<Json::ArrayIndex>(i), (trans_json.empty() ? 0.0f : trans_json[trans_json.size() - 1].asFloat())).asFloat();
            float scat = scat_json.get(static_cast<Json::ArrayIndex>(i), (scat_json.empty() ? 0.0f : scat_json[scat_json.size() - 1].asFloat())).asFloat();

            abs_map[frequencies_[i]] = abs;
            trans_map[frequencies_[i]] = trans;
            scat_map[frequencies_[i]] = scat;
        }
        materials_.insert(std::make_pair(matName, AudioMat(abs_map, trans_map, scat_map)));
    }
}

}; // namespace varays
