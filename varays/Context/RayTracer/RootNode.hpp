// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "../../Common/AudioSource.hpp"
#include "../../Common/SoundEvent.hpp"
#include "./TreeNode.hpp"
#include <algorithm>
#include <Eigen/Dense>
#include <vector>

namespace varays
{
// A TreeNode is an intersection point in the rayTree. It has a location which should always be a on a surface.
struct RootNode
{
    RootNode(const Eigen::Vector3f& coordinates, const std::vector<AudioSource>& sources)
        : coordinates(coordinates)
    {
        std::transform(sources.begin(), sources.end(), std::inserter(energies, energies.end()), [](AudioSource source) -> std::pair<int, varays::energy> {
            return {source.getId(), source.getEnergy()};
        });
    };

    RootNode()
        : RootNode(Eigen::Vector3f(0.0f, 0.0f, 0.0f), std::vector<AudioSource>())
    {
    }

    // Returns the TreeNodes that correspond to the specified order that currently do not have any children
    std::vector<TreeNode*> findEndTreeNodes(TreeNode& node, int returnOrder, int thisOrder)
    {
        if (returnOrder == thisOrder)
        {
            if (node.children.empty())
                return std::vector<TreeNode*>{&node};
            else
                return std::vector<TreeNode*>();
        }
        std::vector<TreeNode*> endNodes;
        for (auto& child : node.children)
        {
            std::vector<TreeNode*> newEndNodes = findEndTreeNodes(child, returnOrder, thisOrder + 1);
            endNodes.insert(endNodes.end(), newEndNodes.begin(), newEndNodes.end());
        }
        return endNodes;
    }

    // Returns the TreeNodes that correspond to the specified order that currently do not have any children
    std::vector<TreeNode*> findEndNodes(int order)
    {
        std::vector<TreeNode*> endNodes;
        for (auto& child : children)
        {
            std::vector<TreeNode*> newEndNodes = findEndTreeNodes(child, order, 1);
            endNodes.insert(endNodes.end(), newEndNodes.begin(), newEndNodes.end());
        }
        return endNodes;
    }

    Eigen::Vector3f coordinates;
    std::vector<TreeNode> children{};
    std::vector<SoundEvent> soundEvents{};
    std::map<int, varays::energy> energies{};

};
}; // namespace varays
