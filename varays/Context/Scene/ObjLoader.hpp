// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Library General Public
// License along with this library; if not, write to the
// Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
// Boston, MA 02110-1301, USA.

// OBJ_Loader.h - A Single Header OBJ Model Loader
// https://github.com/Bly7/OBJ-Loader

#ifndef __VARAYS_OBJLOADER__
#define __VARAYS_OBJLOADER__

#include <cmath>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include <Eigen/Dense>

// Print progress to console while loading (large models)
//#define OBJL_CONSOLE_OUTPUT

// Namespace: OBJL
//
// Description: The namespace that holds eveyrthing that
//	is needed and used for the OBJ Model Loader
namespace objl
{

struct Material
{
    Material()
    {
        name = "";
        Ns = 0.0f;
        Ni = 0.0f;
        d = 0.0f;
        illum = 0;
    }

    /** Material Name */
    std::string name;
    /** Ambient Color */
    Eigen::Vector3f Ka;
    /** Diffuse Color */
    Eigen::Vector3f Kd;
    /** Specular Color */
    Eigen::Vector3f Ks;
    /** Specular Exponent */
    float Ns;
    /** Optical Density */
    float Ni;
    /** Dissolve */
    float d;
    /** Illumination */
    int illum;
    /** Ambient Texture Map */
    std::string map_Ka;
    /** Diffuse Texture Map */
    std::string map_Kd;
    /** Specular Texture Map */
    std::string map_Ks;
    /** Specular Hightlight Map */
    std::string map_Ns;
    /** Alpha Texture Map */
    std::string map_d;
    /** Bump Map */
    std::string map_bump;
};

/** Structure: Mesh
 * Description: A Simple Mesh Object that holds
 *	a name, a vertex list, and an index list
 */
struct Mesh
{
    Mesh() {}
    Mesh(const std::vector<Eigen::Vector3f>& _Vertices, const std::vector<int>& _Indices);

    std::string MeshName;
    std::vector<Eigen::Vector3f> Vertices;
    std::vector<int> Indices;

    Material MeshMaterial;
};

// Namespace: Algorithm
//
// Description: The namespace that holds all of the
// Algorithms needed for OBJL
namespace algorithm
{
// Angle between 2 Eigen::Vector3f Objects
float AngleBetweenV3(const Eigen::Vector3f& a, const Eigen::Vector3f& b);

// Projection Calculation of a onto b
Eigen::Vector3f ProjV3(const Eigen::Vector3f& a, const Eigen::Vector3f& b);

// A test to see if P1 is on the same side as P2 of a line segment ab
bool SameSide(const Eigen::Vector3f& p1, const Eigen::Vector3f& p2, const Eigen::Vector3f& a, const Eigen::Vector3f& b);

// Generate a cross product normal for a triangle
Eigen::Vector3f GenTriNormal(const Eigen::Vector3f& t1, const Eigen::Vector3f& t2, const Eigen::Vector3f& t3);

// Check to see if a Eigen::Vector3f Point is within a 3 Eigen::Vector3f Triangle
bool inTriangle(const Eigen::Vector3f& point, const Eigen::Vector3f& tri1, const Eigen::Vector3f& tri2, const Eigen::Vector3f& tri3);

// Split a String into a string array at a given token
void split(const std::string& in, std::vector<std::string>& out, const std::string& token);

// Get tail of string after first token and possibly following spaces
std::string tail(const std::string& in);

// Get first token of string
std::string firstToken(const std::string& in);

// Get element at given index position
template <class T> const T& getElement(const std::vector<T>& elements, const std::string& index);
} // namespace algorithm

// Class: Loader
//
// Description: The OBJ Model Loader
class Loader
{
  public:
    // Default Constructor
    Loader() {}

    /** Load a file into the loader
     *
     * If file is loaded return true
     *
     * If the file is unable to be found
     * or unable to be loaded return false
     */
    bool LoadFile(const std::string& Path);

    std::vector<Eigen::Vector3f> LoadedVertices;
    std::vector<int> LoadedIndices;
    std::vector<Material> LoadedMaterials;
    std::vector<Mesh> LoadedMeshes;

  private:
    /** Generate vertices from a list of positions,
     *	tcoords, normals and a face line
     */
    void addIndices(std::vector<int>& iIndices, const std::string& icurline);

    bool LoadMaterials(std::string path);
};
} // namespace objl

#endif
