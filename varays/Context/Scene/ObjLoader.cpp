// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Library General Public
// License along with this library; if not, write to the
// Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
// Boston, MA 02110-1301, USA.

// OBJ_Loader.h - A Single Header OBJ Model Loader
// Copyright (c) 2016 Robert Smith
// https://github.com/Bly7/OBJ-Loader

#include "./ObjLoader.hpp"

objl::Mesh::Mesh(const std::vector<Eigen::Vector3f>& _Vertices, const std::vector<int>& _Indices)
    : Vertices(_Vertices)
    , Indices(_Indices)
{
}

float objl::algorithm::AngleBetweenV3(const Eigen::Vector3f& a, const Eigen::Vector3f& b)
{
    float angle = a.dot(b);
    angle /= a.norm() * b.norm();
    return angle = acosf(angle);
}

// Projection Calculation of a onto b
Eigen::Vector3f objl::algorithm::ProjV3(const Eigen::Vector3f& a, const Eigen::Vector3f& b)
{
    Eigen::Vector3f bn = b / b.norm();
    return bn * a.dot(bn);
}

bool objl::algorithm::SameSide(const Eigen::Vector3f& p1, const Eigen::Vector3f& p2, const Eigen::Vector3f& a, const Eigen::Vector3f& b)
{
    Eigen::Vector3f cp1 = (b - a).cross(p1 - a);
    Eigen::Vector3f cp2 = (b - a).cross(p2 - a);

    if (cp1.dot(cp2) >= 0)
        return true;
    else
        return false;
}

Eigen::Vector3f objl::algorithm::GenTriNormal(const Eigen::Vector3f& t1, const Eigen::Vector3f& t2, const Eigen::Vector3f& t3)
{
    Eigen::Vector3f u = t2 - t1;
    Eigen::Vector3f v = t3 - t1;

    Eigen::Vector3f normal = u.cross(v);

    return normal;
}

bool objl::algorithm::inTriangle(const Eigen::Vector3f& point, const Eigen::Vector3f& tri1, const Eigen::Vector3f& tri2, const Eigen::Vector3f& tri3)
{
    // Test to see if it is within an infinite prism that the triangle outlines.
    bool within_tri_prisim = SameSide(point, tri1, tri2, tri3) && SameSide(point, tri2, tri1, tri3) && SameSide(point, tri3, tri1, tri2);

    // If it isn't it will never be on the triangle
    if (!within_tri_prisim)
        return false;

    // Calulate Triangle's Normal
    Eigen::Vector3f n = GenTriNormal(tri1, tri2, tri3);

    // Project the point onto this normal
    Eigen::Vector3f proj = objl::algorithm::ProjV3(point, n);

    // If the distance from the triangle to the point is 0
    //	it lies on the triangle
    if (proj.norm() != 0)
        return false;
    return true;
}

void objl::algorithm::split(const std::string& in, std::vector<std::string>& out, const std::string& token)
{
    out.clear();

    std::string temp;

    for (int i = 0; i < int(in.size());)
    {
        std::string test = in.substr(i, token.size());

        if (test == token)
        {
            if (!temp.empty())
            {
                out.push_back(temp);
                temp.clear();
                i += (int)token.size();
            }
            else
            {
                out.push_back("");
                ++i;
            }
        }
        else if (i + token.size() >= in.size())
        {
            temp += in.substr(i, token.size());
            out.push_back(temp);
            break;
        }
        else
        {
            temp += in[i];
            ++i;
        }
    }
}

std::string objl::algorithm::tail(const std::string& in)
{
    size_t token_start = in.find_first_not_of(" \t");
    size_t space_start = in.find_first_of(" \t", token_start);
    size_t tail_start = in.find_first_not_of(" \t", space_start);
    size_t tail_end = in.find_last_not_of(" \t");
    if (tail_start != std::string::npos && tail_end != std::string::npos)
    {
        return in.substr(tail_start, tail_end - tail_start + 1);
    }
    else if (tail_start != std::string::npos)
    {
        return in.substr(tail_start);
    }
    return "";
}

std::string objl::algorithm::firstToken(const std::string& in)
{
    if (in.empty())
        return "";
    size_t token_start = in.find_first_not_of(" \t");
    size_t token_end = in.find_first_of(" \t", token_start);
    if (token_start != std::string::npos && token_end != std::string::npos)
    {
        return in.substr(token_start, token_end - token_start);
    }
    else if (token_start != std::string::npos)
    {
        return in.substr(token_start);
    }
    else
        return "";
}

template <class T> const T& objl::algorithm::getElement(const std::vector<T>& elements, const std::string& index)
{
    int idx = std::stoi(index);
    if (idx < 0)
        idx = int(elements.size()) + idx;
    else
        idx--;
    return elements[idx];
}

bool objl::Loader::LoadFile(const std::string& Path)
{
    // If the file is not an .obj file return false
    if (Path.substr(Path.size() - 4, 4) != ".obj")
        return false;

    std::ifstream file(Path);

    if (!file.is_open())
        return false;

    LoadedMeshes.clear();
    LoadedVertices.clear();
    LoadedIndices.clear();

    std::vector<Eigen::Vector3f> Vertices;
    std::vector<int> Indices;

    std::vector<std::string> MeshMatNames;

    bool listening = false;
    std::string meshname;

    Mesh tempMesh;

#ifdef OBJL_CONSOLE_OUTPUT
    const unsigned int outputEveryNth = 1000;
    unsigned int outputIndicator = outputEveryNth;
#endif

    std::string curline;
    while (std::getline(file, curline))
    {
#ifdef OBJL_CONSOLE_OUTPUT
        if ((outputIndicator = ((outputIndicator + 1) % outputEveryNth)) == 1)
        {
            if (!meshname.empty())
            {
                std::cout << "\r- " << meshname << "\t| vertices > " << Vertices.size() << (!MeshMatNames.empty() ? "\t| material: " + MeshMatNames.back() : "")
                          << '\n';
            }
        }
#endif

        // Generate a Mesh Object or Prepare for an object to be created
        if (algorithm::firstToken(curline) == "o" || algorithm::firstToken(curline) == "g" || curline[0] == 'g')
        {
            if (!listening)
            {
                listening = true;

                if (algorithm::firstToken(curline) == "o" || algorithm::firstToken(curline) == "g")
                {
                    meshname = algorithm::tail(curline);
                }
                else
                {
                    meshname = "unnamed";
                }
            }
            else
            {
                // Generate the mesh to put into the array

                if (!Indices.empty() && !Vertices.empty())
                {
                    // Create Mesh
                    tempMesh = Mesh(Vertices, Indices);
                    tempMesh.MeshName = meshname;

                    // Insert Mesh
                    LoadedMeshes.push_back(tempMesh);

                    // Cleanup
                    Vertices.clear();
                    Indices.clear();
                    meshname.clear();

                    meshname = algorithm::tail(curline);
                }
                else
                {
                    if (algorithm::firstToken(curline) == "o" || algorithm::firstToken(curline) == "g")
                    {
                        meshname = algorithm::tail(curline);
                    }
                    else
                    {
                        meshname = "unnamed";
                    }
                }
            }
#ifdef OBJL_CONSOLE_OUTPUT
            std::cout << std::endl;
            outputIndicator = 0;
#endif
        }
        // Generate a Vertex Position
        if (algorithm::firstToken(curline) == "v")
        {
            std::vector<std::string> spos;
            Eigen::Vector3f vpos;
            algorithm::split(algorithm::tail(curline), spos, " ");
            auto it = spos.begin();
            while (it != spos.end())
            {
                if (*it == " " || *it == "")
                {
                    spos.erase(it);
                }
                else
                {
                    ++it;
                }
            }

            vpos.x() = std::stof(spos[0]);
            vpos.y() = std::stof(spos[1]);
            vpos.z() = std::stof(spos[2]);

            Vertices.push_back(vpos);
        }

        // Generate a Face (vertices & indices)
        if (algorithm::firstToken(curline) == "f")
        {
            // Generate the vertices
            addIndices(Indices, curline);
        }
        // Get Mesh Material Name
        if (algorithm::firstToken(curline) == "usemtl")
        {
            MeshMatNames.push_back(algorithm::tail(curline));

            // Create new Mesh, if Material changes within a group
            if (!Indices.empty() && !Vertices.empty())
            {
                // Create Mesh
                tempMesh = Mesh(Vertices, Indices);
                tempMesh.MeshName = meshname;
                int i = 2;
                while (1)
                {
                    tempMesh.MeshName = meshname + "_" + std::to_string(i);

                    for (auto& m : LoadedMeshes)
                        if (m.MeshName == tempMesh.MeshName)
                            continue;
                    break;
                }

                // Insert Mesh
                LoadedMeshes.push_back(tempMesh);

                // Cleanup
                Vertices.clear();
                Indices.clear();
            }

#ifdef OBJL_CONSOLE_OUTPUT
            outputIndicator = 0;
#endif
        }
        // Load Materials
        if (algorithm::firstToken(curline) == "mtllib")
        {
            // Generate LoadedMaterial

            // Generate a path to the material file
            std::vector<std::string> temp;
            algorithm::split(Path, temp, "/");
            temp.pop_back();

            std::string pathtomat = "";

            if (temp.size() != 1)
            {
                for (const auto& tempStr : temp)
                {
                    pathtomat += tempStr + "/";
                }
            }

            pathtomat += algorithm::tail(curline);

#ifdef OBJL_CONSOLE_OUTPUT
            std::cout << std::endl << "- find materials in: " << pathtomat << std::endl;
#endif

            // Load Materials
            LoadMaterials(pathtomat);
        }
    }

#ifdef OBJL_CONSOLE_OUTPUT
    std::cout << std::endl;
#endif

    // Deal with last mesh
    if (!Indices.empty() && !Vertices.empty())
    {
        // Create Mesh
        tempMesh = Mesh(Vertices, Indices);
        tempMesh.MeshName = meshname;

        // Insert Mesh
        LoadedMeshes.push_back(tempMesh);
    }

    file.close();

    // Set Materials for each Mesh
    for (unsigned int i = 0; i < MeshMatNames.size(); ++i)
    {
        std::string matname = MeshMatNames[i];
        LoadedMeshes[i].MeshMaterial.name = MeshMatNames[i]; // When no .mtl file is found, only the name is assigned

        // Find corresponding material name in loaded materials
        // when found copy material variables into mesh material
        for (const auto& loadedMaterial : LoadedMaterials)
        {
            if (loadedMaterial.name == matname)
            {
                LoadedMeshes[i].MeshMaterial = loadedMaterial;
                break;
            }
        }
    }

    if (LoadedMeshes.empty() && LoadedVertices.empty() && LoadedIndices.empty())
    {
        return false;
    }
    return true;
}

void objl::Loader::addIndices(std::vector<int>& iIndices, const std::string& icurline)
{
    std::vector<std::string> sface, svert;
    algorithm::split(algorithm::tail(icurline), sface, " ");

    if (sface.size() == 3)
    {
        for (const auto& sf : sface)
        {
            algorithm::split(sf, svert, "/");
            iIndices.push_back(std::stoi(svert[0]) - 1);
        }
    }
    else if (sface.size() > 3)
    {
        algorithm::split(sface[0], svert, "/");
        int fIndice = std::stoi(svert[0]) - 1;
        sface.erase(sface.begin());
        for (unsigned int i = 0; i < sface.size() - 1; ++i)
        {
            iIndices.push_back(fIndice);
            algorithm::split(sface[i], svert, "/");
            iIndices.push_back(std::stoi(svert[0]) - 1);
            algorithm::split(sface[i + 1], svert, "/");
            iIndices.push_back(std::stoi(svert[0]) - 1);
        }
    }
}

bool objl::Loader::LoadMaterials(std::string path)
{
    // If the file is not a material file return false
    if (path.substr(path.size() - 4, path.size()) != ".mtl")
        return false;

    std::ifstream file(path);

    // If the file is not found return false
    if (!file.is_open())
        return false;

    Material tempMaterial;

    bool listening = false;

    // Go through each line looking for material variables
    std::string curline;
    while (std::getline(file, curline))
    {
        // new material and material name
        if (algorithm::firstToken(curline) == "newmtl")
        {
            if (!listening)
            {
                listening = true;

                if (curline.size() > 7)
                {
                    tempMaterial.name = algorithm::tail(curline);
                }
                else
                {
                    tempMaterial.name = "none";
                }
            }
            else
            {
                // Generate the material

                // Push Back loaded Material
                LoadedMaterials.push_back(tempMaterial);

                // Clear Loaded Material
                tempMaterial = Material();

                if (curline.size() > 7)
                {
                    tempMaterial.name = algorithm::tail(curline);
                }
                else
                {
                    tempMaterial.name = "none";
                }
            }
        }
        // Ambient Color
        if (algorithm::firstToken(curline) == "Ka")
        {
            std::vector<std::string> temp;
            algorithm::split(algorithm::tail(curline), temp, " ");

            if (temp.size() != 3)
                continue;

            tempMaterial.Ka.x() = std::stof(temp[0]);
            tempMaterial.Ka.y() = std::stof(temp[1]);
            tempMaterial.Ka.z() = std::stof(temp[2]);
        }
        // Diffuse Color
        if (algorithm::firstToken(curline) == "Kd")
        {
            std::vector<std::string> temp;
            algorithm::split(algorithm::tail(curline), temp, " ");

            if (temp.size() != 3)
                continue;

            tempMaterial.Kd.x() = std::stof(temp[0]);
            tempMaterial.Kd.y() = std::stof(temp[1]);
            tempMaterial.Kd.z() = std::stof(temp[2]);
        }
        // Specular Color
        if (algorithm::firstToken(curline) == "Ks")
        {
            std::vector<std::string> temp;
            algorithm::split(algorithm::tail(curline), temp, " ");

            if (temp.size() != 3)
                continue;

            tempMaterial.Ks.x() = std::stof(temp[0]);
            tempMaterial.Ks.y() = std::stof(temp[1]);
            tempMaterial.Ks.z() = std::stof(temp[2]);
        }
        // Specular Exponent
        if (algorithm::firstToken(curline) == "Ns")
        {
            tempMaterial.Ns = std::stof(algorithm::tail(curline));
        }
        // Optical Density
        if (algorithm::firstToken(curline) == "Ni")
        {
            tempMaterial.Ni = std::stof(algorithm::tail(curline));
        }
        // Dissolve
        if (algorithm::firstToken(curline) == "d")
        {
            tempMaterial.d = std::stof(algorithm::tail(curline));
        }
        // Illumination
        if (algorithm::firstToken(curline) == "illum")
        {
            tempMaterial.illum = std::stoi(algorithm::tail(curline));
        }
        // Ambient Texture Map
        if (algorithm::firstToken(curline) == "map_Ka")
        {
            tempMaterial.map_Ka = algorithm::tail(curline);
        }
        // Diffuse Texture Map
        if (algorithm::firstToken(curline) == "map_Kd")
        {
            tempMaterial.map_Kd = algorithm::tail(curline);
        }
        // Specular Texture Map
        if (algorithm::firstToken(curline) == "map_Ks")
        {
            tempMaterial.map_Ks = algorithm::tail(curline);
        }
        // Specular Hightlight Map
        if (algorithm::firstToken(curline) == "map_Ns")
        {
            tempMaterial.map_Ns = algorithm::tail(curline);
        }
        // Alpha Texture Map
        if (algorithm::firstToken(curline) == "map_d")
        {
            tempMaterial.map_d = algorithm::tail(curline);
        }
        // Bump Map
        if (algorithm::firstToken(curline) == "map_Bump" || algorithm::firstToken(curline) == "map_bump" || algorithm::firstToken(curline) == "bump")
        {
            tempMaterial.map_bump = algorithm::tail(curline);
        }
    }

    // Deal with last material

    // Push Back loaded Material
    LoadedMaterials.push_back(tempMaterial);

    // Test to see if anything was loaded
    // If not return false
    if (LoadedMaterials.empty())
        return false;
    return true;
}
