# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import time
from osc_sender import OscSender

sender = OscSender(9000)
sender.send_user_position(0.0, 0.0, 0.0)
time.sleep(0.15)

sender.create_new_source("sound", 2.0, 0.5, 0.5)

for i in range(0, 11):
    sender.move_source(
        "sound",
        float(round(i*0.4 - 2, 1)),
        0.5,
        0.5
    )
    time.sleep(0.5)

sender.remove_source("sound")
sender.quit()
print("Done!")
