"""Python script to run pyvarays. Duplicates the functions of main.cpp."""

from argparse import ArgumentParser
import json
import logging
import os
import sys

import liblo
import numpy as np

import pyvarays as pv

DATA_DIR = os.path.realpath(os.path.join(
    os.path.dirname(__file__),
    "res"
))

logging.basicConfig(format='%(levelname)s: %(message)s')

logger = logging.getLogger(__name__)


class Params:
    """Class for setting and storing VaRays parameters."""
    def __init__(self, config_file: str, args: ArgumentParser = None,
                 auralizer_params: pv.VaraysAuralizerParams = None):
        # Load params from config file.
        with open(config_file) as fp:
            config_json: dict = json.load(fp)

        # Set parameters from config file first.
        self.enable_auralizer: bool = config_json.get("EnableAuralizer") or False
        self.enable_doppler_shift: bool = config_json.get("EnableDopplerShift") or True
        self.jack_server_name: str = config_json.get("JackServerName") or "default"
        self.delay_line_interpolation_order: int = config_json.get("DelayLineInterpolationOrder") or 2
        self.max_relative_velocity: float = config_json.get("MaxRelativeVelocity") or 50.0
        self.ambisonic_order: int = config_json.get("AmbisonicOrder") or 0
        self.max_audio_sources: int = config_json.get("MaxAudioSource") or 5
        # Sampling frequency for the generated IR.
        self.sample_rate: int = config_json.get("SampleRate") or 48000
        # Temperature in celsius.
        self.temperature: float = config_json.get("Temperature") or 20.0
        # Percent humidity.
        self.humidity: float = config_json.get("Humidity") or 50.0
        # Pressure in kPa.
        self.pressure: float = config_json.get("Pressure") or 101.325
        self.enable_ray_cache: bool = config_json.get("EnableRayCache") or False
        self.enable_diffraction: bool = config_json.get("EnableDiffraction") or False
        # Number of "original" rays shot from the user location.
        self.init_rays: int = config_json.get("InitRays") or 500
        # Number of reflected rays for every reflection on a surface.
        self.num_reflected_rays: int = config_json.get("NumReflectedRays") or 3
        # Maximum number of reflections.
        self.max_reflection_order: int = config_json.get("MaxReflectionOrder") or 8
        # After this many reflections, all reflections will only be specular.
        self.max_diffuse_order: int = config_json.get("MaxDiffuseOrder") or 3
        # Threshold of energy (fraction of its initial ray) a ray must have in
        # order to be thrown.
        self.min_energy_threshold: float = config_json.get("MinEnergyThreshold") or 0.005
        # Minimum time (ms) taken before generating a new Impulse Response
        # (it can take longer if the calculations take too long)
        self.delay: int = config_json.get("RTPeriod") or 100
        # Port for receiving osc messages.
        self.osc_in_port: int = config_json.get("OSCInPort") or 9000
        # Port for sending osc messages.
        self.osc_out_port: int = config_json.get("OSCOutPort") or 7770
        self.ir_path: str = config_json.get("IRPath") or None
        self.ray_obj_path: str = config_json.get("RayObjPath") or None
        # Path of the obj file used as a scene.
        self.obj_file: str = config_json.get("ObjFile") or None

        # Params for compute_ir function.
        # Dictionary of sources to add to scene on every loop.
        # { source ID: source object}
        self.sources_to_add: dict = {}
        # List of names of sources to remove on every loop.
        self.sources_to_remove: list = []
        # Dict of source positions { source ID: np array  of position (x, y, z)}
        self.source_positions: dict = {}
        # Boolean, set to True if all sources should be removed.
        self.remove_all_sources: bool = False

        # Initialize vaRays objects.
        self.context = pv.VaraysContext(
            os.path.join(DATA_DIR, "Config/materials.json"),
            np.array([-0.5, 1.0, -1.5]),
            self.enable_ray_cache
        )

        self.encoder = pv.VaraysEncoder(
            self.context.get_frequencies(),
            self.sample_rate
        )

        self.encoder.set_atmospheric_conditions(
            self.humidity,
            self.pressure,
            self.temperature
        )

        # Handle command line arguments (take priority over config file params).
        if args is not None:
            if args.a is not None:
                self.ambisonic_order = args.a

            if args.C:
                self.enable_auralizer = True

            if args.i is not None:
                self.ir_path = args.i

            if args.o is not None:
                self.obj_file = args.o

            if args.r is not None:
                self.ray_obj_path = args.r

        # Varays Auralizer construction
        if self.enable_auralizer:
            if auralizer_params is None:
                auralizer_params = pv.VaraysAuralizerParams(
                    jack_server=self.jack_server_name,
                    num_inputs=self.max_audio_sources,
                    num_outputs=(self.ambisonic_order + 1)**2,
                    convolver_size=self.sample_rate * 2,
                    enable_doppler=self.enable_doppler_shift,
                    doppler_interpolation_order=self.delay_line_interpolation_order,
                    doppler_max_velocity=self.max_relative_velocity,
                    doppler_speed_of_sound=self.encoder.get_speed_of_sound(),
                )

            self.auralizer = pv.VaraysAuralizer(auralizer_params)

            if not self.auralizer.is_allocated():
                logger.error("Construction of VaraysAuralizer failed. VaraysAuralizer disabled.")
                self.enable_auralizer = False
            else:
                # Enable SIMD vectorized MAC operations in the auralizer.
                self.auralizer.set_vectorized_operations(True)

                self.sample_rate = self.auralizer.get_sample_rate()

                # Update sample rate of the encoder, triggers recomputation of
                # filter coefficients.
                self.encoder.set_sample_rate(self.sample_rate)

        # Log warnings.
        if self.ir_path is None:
            logger.warning("IRs will not be saved since no path is provided.")
        if self.ray_obj_path is None:
            logger.warning((
                ".obj file containing visible rays will not be saved since no "
                "path is provided."
            ))


def init_arg_parser() -> ArgumentParser:
    """Initiate command line arguments for arg parser."""
    parser = ArgumentParser()

    parser.add_argument(
        "-a",
        metavar="ORDER",
        type=int,
        help="Ambisonic order of the output (uses SN3D) (Default=0)."
    )
    parser.add_argument(
        "-C",
        help="Enable Varays Auralizer (Requires an active Jack server).",
        action="store_true"
    )
    parser.add_argument(
        "-i",
        metavar="PATH",
        type=str,
        help=(
            "Relative path of the generated IR file (Defaults to value in "
             "config file. if no value is set in config file, IRs will not be "
             " saved)."
        )
    )
    parser.add_argument(
        "-o",
        metavar="PATH",
        type=str,
        help="Path of the obj file used as a scene (Default will use a 2x2x2 cube centered in (0,0,0))."
    )
    parser.add_argument(
        "-r",
        metavar="PATH",
        type=str,
        help=(
            "Relative path of the .obj file containing rays visible to the "
            "sources (Defaults to value in config file. if no value is set in "
            "config file, .obj file will not be saved)."
        )
    )
    parser.add_argument(
        "-c",
        metavar="PATH",
        type=str,
        help=(
            "Config file path"
        )
    )

    return parser


def add_cube(varays_context: pv.VaraysContext, diffraction: bool = True,
             center: np.ndarray = np.array([0., 0., 0.]), length=1.) -> None:
    """Helper Function to add a 2x2x2 cube centered at (0,0,0) to the 
    Varays Context."""
    vertices = np.zeros((9, 3))

    for i in range(9):
        vertex = np.array([-1 ** (i / 4.), -1 ** (i / 2.), -1 ** i])
        vertex *= (length / 2.)

        vertices[i] = center + vertex

    indices = np.array([
        0, 1, 2, 1, 3, 2, 4, 6, 5,
        5, 6, 7, 0, 4, 1, 1, 4, 5,
        2, 3, 6, 3, 7, 6, 0, 2, 4,
        2, 6, 4, 1, 5, 3, 3, 5, 7
    ])

    varays_context.add_geom(vertices, indices, diffraction)


def compute_ir(params: Params) -> None:
    """Helper function called inside of loop to update scene."""
    # Add / move / remove audio sources.
    for key, val in params.sources_to_add.items():
        params.context.add_source(val, key)
        if params.enable_auralizer:
            params.auralizer.new_source(key)

    params.sources_to_add.clear()

    for key, val in params.source_positions.items():
        params.varays_context.move_source(
            params.varays_context.get_source_id_from_name(key),
            val
        )

    params.sources_to_add.clear()

    if (params.remove_all_sources):
        params.sources_to_remove = [source.get_name() for source in params.context.get_sources()]

        params.remove_all_sources = False

    for id in params.sources_to_remove:
        params.context.remove_source(
            params.context.get_source_id_from_name(id)
        )
        if params.enable_auralizer:
            params.auralizer.delete_source(id)

    params.sources_to_remove.clear()

    if not params.context.get_sources():
        return
    
    params.context.add_ray_manager(
        0, 
        params.max_reflection_order, 
        1,
        params.min_energy_threshold
    )

    sound_events = params.context.execute_ray_managers(
        params.init_rays,
        params.num_reflected_rays,
        params.max_diffuse_order
    )

    if not sound_events:
        return

    ir_for_sources = params.encoder.write_ir(
        params.ambisonic_order,
        sound_events,
        params.enable_doppler_shift
    )

    sources = params.context.get_sources()

    # Update Impulse Response for all audio sources.
    if params.enable_auralizer:
        # For each updated Impulse Response, find the matching Audio Source.
        for ir_id, ir_vals in ir_for_sources.items():
            # Find the Audio Source with matching ID.
            source = next((s for s in sources if s.get_id() == ir_id), None)
            if source is not None:
                # Update with the new Impulse Response.
                params.auralizer.update_ir(
                    source.get_name(),
                    (params.ambisonic_order+1)**2,
                    ir_vals
                )

    # Write IRs to files.
    if params.ir_path is not None:
        for ir_id, ir_vals in ir_for_sources.items():
            # Find the audio source with matching ID.
            source = next((s for s in sources if s.get_id() == ir_id), None)
            if source is not None:
                path = params.ir_path + source.get_name()
                params.varays_encoder.export_ir(
                    ir_vals,
                    path,
                    (params.ambisonic_order+1)**2,
                    params.sample_rate
                )

                # TODO: send osc message to satie

    # Export rays to .obj
    if params.ray_obj_path is not None:
        params.varays_context.write_rays_to_obj(params.ray_obj_path)


def main(argv: list = []) -> None:
    parser = init_arg_parser()
    args = parser.parse_args(argv)

    if args is not None and args.c: # Read from config file in CLI or use default file
        param_file = args.c
    else:
        param_file = os.path.join(DATA_DIR, "Config/vaRaysAppConfig.json")
    params = Params(param_file, args)

    # Boolean status variable,
    # when True, pyvarays should loop and poll for osc messages,
    # set to False when pyvarays should quit.
    loop = True

    # Callbacks.
    def toggle_loop(path, argv, types, data):
        nonlocal loop
        loop = not loop

    def add_source(path, argv, types, data):
        nonlocal params
        params.sources_to_add[argv[0]] = np.array([argv[1], argv[2], argv[3]])

    def move_source(path, argv, types, data):
        nonlocal params
        params.source_positions[argv[0]] = np.array([argv[1], argv[2], argv[3]])

    def remove_source(path, argv, types, data):
        nonlocal params
        params.sources_to_remove.append(argv[0])

    def remove_all_sources(path, argv, types, data):
        nonlocal params
        params.remove_all_sources = True

    # Start OSC server for receiving messages.
    osc_server = liblo.Server(params.osc_in_port)

    osc_server.add_method("/quit", "", toggle_loop)

    osc_server.add_method("/new_source", "sfff", add_source)

    osc_server.add_method("/move_source", "sfff", move_source)

    osc_server.add_method("/remove_source", "s", remove_source)

    osc_server.add_method("/remove_all_sources", "", remove_all_sources)

    while loop:
        # Send out a message to say the server is running.
        osc_server.send(3000, "/is_running", 1)

        compute_ir(params)
        osc_server.recv(100)


if __name__ == "__main__":
    main(sys.argv[1:])
